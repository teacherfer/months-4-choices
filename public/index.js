var animals = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var length = animals.length;
var unselectedAnimals = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var correctAnswersCounters;
var incorrectAnswersCounters;
var posibleAnswers = [];
var currentPosition = -1;
var speech = null;
var answered = false;

initAnswersCountersArrays();
randomizeQuestion();

function initAnswersCountersArrays() {
    correctAnswersCounters = new Array(length).fill(0);
    incorrectAnswersCounters = new Array(length).fill(0);
}

function marcador() {
    for (let index = 0; index < length; index++) {
        console.log(animals[index] + " -> Aciertos " + correctAnswersCounters[index] + " Fallos " + incorrectAnswersCounters[index]);
    }
}

function randomizeQuestion() {
    var rand = Math.floor(Math.random() * unselectedAnimals.length);
    currentPosition = animals.indexOf(unselectedAnimals[rand]);
    unselectedAnimals.splice(rand, 1);
    loadQuestionImage();
    randomizeAnswers();
}

function loadQuestionImage() {
    const image = document.createElement("img");
    image.src = "img/" + animals[currentPosition].toLowerCase() + ".jpg";
    document.querySelector(".speaker-container").appendChild(image);
}

function randomizeAnswers() {
    posibleAnswers = [];
    var animalsClone = animals.slice(0);
    posibleAnswers.push(animals[currentPosition]);
    animalsClone.splice(currentPosition, 1);
    animalsClone.sort(() => .5 - Math.random());
    var aux = animalsClone.slice(0, 3);
    posibleAnswers = posibleAnswers.concat(aux);
    posibleAnswers.sort(() => .5 - Math.random());
    loadAnswerButtons();
}

function loadAnswerButtons() {
    for (let index = 0; index < posibleAnswers.length; index++) {
        const element = document.createElement("button");
        element.innerHTML = posibleAnswers[index];
        element.value = posibleAnswers[index];
        element.classList.add("answer");
        document.querySelector(".answers-container").appendChild(element);
    }
    loadAnswersListeners();
}

function loadAnswersListeners() {
    var answers = document.querySelectorAll(".answer");
    for (let i = 0; i < answers.length; i++) {
        answers[i].addEventListener("click", answerClicked);
    }
}

function loadSpeech() {
    speech = new SpeechSynthesisUtterance(animals[currentPosition]);
    speech.lang = 'en-UK';
    document.querySelector(".fa-volume-up").addEventListener("click", playSpeech);
}

function playSpeech() {
    if (!answered)
    window.speechSynthesis.speak(speech);
}

function answerClicked(e) {
    if (!answered) {
        answered = true;
        var answerText = document.querySelector(".answer-text");
        answerText.style.opacity = 1;
        var selected = e.target.value;
        var answerIndex = animals.indexOf(selected);
        answerText.textContent = selected;
        if (answerIndex == currentPosition) {
            answerText.style.color = 'green';
            correctAnswersCounters[answerIndex]++;
        } else {
            answerText.style.color = 'red';
            incorrectAnswersCounters[currentPosition]++;
        }
        var s = answerText.style,
            step = 25 / (1500 || 300);
        s.opacity = s.opacity || 1;
        (function fade() {
            (s.opacity -= step) < 0 ? s.textContent = "" : setTimeout(fade, 25);
        })();
        setTimeout(nextQuestion, 1000);
    }
}

function nextQuestion() {
    answered = false;
    removePosibleAnswers();
    randomizeQuestion();
    if (unselectedAnimals.length == 0)
        unselectedAnimals = animals.slice(0);

}

function removePosibleAnswers() {
    document.querySelector(".answers-container").innerHTML = "";
    var elem = document.querySelector(".speaker-container img");
    elem.parentNode.removeChild(elem);
}
